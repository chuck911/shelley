const read = require('read-art')
const Redis = require('ioredis')
const bull = require('bull')
const got = require('got')
const striptags = require('striptags')
const Url = require('url')
const ADLER32 = require('adler-32')

const redis = new Redis()
const crawlQueue = new bull('crawl')

const defaultContent = {
	title: '百姓网',
	content: '百姓网致力于打造以分类信息业务为平台、多条垂直业务线布局的生态圈，为用户提供涵盖生活服务、招聘求职、房屋租售、二手车买卖、二手交易、教育培训、同城交友等本地生活解决方案。'
}

const getKeyOf = {
	hash: (url) => 'shelley:contents:'+url.host,
	set: (url) => 'shelley:urls:'+url.host
}

const refreshTTL = (url) => {
	redis.expire(getKeyOf.hash(url), 7 * 24 * 60 * 60)
	redis.expire(getKeyOf.set(url), 7 * 24 * 60 * 60)
}

const getContent = async (_url) => {
	if (!_url) return defaultContent
	const url = new Url.parse(_url)
	try {
		const key = Math.abs(ADLER32.str(url.href)) % 10000
		let content = await redis.hget(getKeyOf.hash(url), key)
		refreshTTL(url)
		if (content) {
			return JSON.parse(content)
		} else {
			crawlQueue.add({url: url.href})
			const randomKey = await redis.srandmember(getKeyOf.set(url))
			content = await redis.hget(getKeyOf.hash(url), randomKey)
			return content ? JSON.parse(content) : defaultContent
		}
	} catch (error) {
		console.log(error)
	}
}

const crawl = async (url) => {
	const response = await got(url.href)
	const _article = await read(response.body)
	const article = {title: _article.title, content: striptags(_article.content)}
	if (article.title && article.content) {
		const key = Math.abs(ADLER32.str(url.href)) % 10000
		await redis.sadd(getKeyOf.set(url), key)
		await redis.hset(getKeyOf.hash(url), key, JSON.stringify(article))
		refreshTTL(url)
		return article;
	}
}

module.exports = {
	getContent,
	crawl
}