const micro = require('micro')
const fs = require('fs')
const { router, get } = require('microrouter')
const template = require('./template')
const spider = require('./spider')

function getAd(id) {
	return new Promise(function (resolve, reject) {
		fs.readFile('./mockdata.json', (err, data) => {
			if (err) {
				reject(err)
			} else {
				const ad = JSON.parse(data)
				ad.id = id
				resolve(ad)
			}
		});
	});
}

const inner = async (req, res) => {
	const ad = await getAd(req.params.id)
	return template.inner(ad.code)
}

const wrapper = async (req, res) => {
	const referer = req.query._referer || req.headers.referer
	const article = await spider.getContent(referer)
	const ad = await getAd(req.params.id)
	return template.wrapper(ad, article)
}

const server = micro(router(
	get('/i/:id.html', inner),
	get('/w/:id.html', wrapper),
))

server.listen(3333)