const bull = require('bull')
const Url = require('url')
const crawlQueue = new bull('crawl')
const Redis = require('ioredis')
const redis = new Redis()
const spider = require('./spider')
const ADLER32 = require('adler-32')

crawlQueue.process(async (job) => {
    const url = Url.parse(job.data.url)
    const key = Math.abs(ADLER32.str(url.href)) % 10000
    const keyOfHash = 'shelley:contents:'+url.host
    const content = await redis.hget(keyOfHash, key)
    if (!content) {
        return spider.crawl(url)
    }
})