const inner = code => `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>baidu ad</title>
		<style>* { padding: 0; margin: 0; width:100%; }</style>
	</head>
	<body>
		${code}
	</body>
</html>
`
const wrapper = (ad, article) => `
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>${article ? article.title : ''}</title>
		<style>* { padding: 0; margin: 0; width:100%; }</style>
	</head>
	<body>
		<iframe src="/i/${ad.id}.html" style="width:${ad.width}px;height:${ad.height}px;" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
		<h1>${article ? article.title : ''}</h1>
		<div>
		${article ? article.content : ''}
		</div>
	</body>
</html>
`

module.exports = {
	inner,
	wrapper
}